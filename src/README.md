# Just a Placeholder

This directory could contain all the source code relevant to your project, for example
the templates and scripts necessary to turn the data from `/content` into a full website.

Mattrbld does not enforce a particular directory structure, so just see this as an example.
